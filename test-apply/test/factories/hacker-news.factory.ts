import { faker } from '@faker-js/faker';
import { Factory } from 'rosie';
import { HackerNewsDocument } from 'src/modules/hacker-news/schemas/hacker-news.schema';

export const HackerNewsFactory = new Factory<HackerNewsDocument>()
  .attr('object_id', () => faker.name.firstName())
  .attr('title', () => faker.datatype.string())
  .attr('story_title', () => faker.datatype.string())
  .attr('url', () => faker.internet.url())
  .attr('story_url', () => faker.internet.url())
  .attr('author', () => faker.name.fullName())
  .attr('shouldDisplay', () => faker.datatype.boolean())
  .attr('created_at', () => faker.date.past());
