import { Module } from '@nestjs/common';
import { ScheduleModule } from '@nestjs/schedule';
import { HackerNewsModule } from './modules/hacker-news/hacker-news.module';
import { MongooseModule } from '@nestjs/mongoose';
import { ConfigModule } from '@nestjs/config';

@Module({
  imports: [
    ConfigModule.forRoot(),
    ScheduleModule.forRoot(),
    HackerNewsModule,
    MongooseModule.forRoot(process.env.mongoDBUrl),
  ],
})
export class AppModule {}
