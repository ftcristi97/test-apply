import { HttpService } from '@nestjs/axios';
import { Injectable } from '@nestjs/common';
import { AxiosResponse } from 'axios';
import { Cron } from '@nestjs/schedule';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { HackerNews } from '../schemas/hacker-news.schema';

@Injectable()
export class HackerNewsService {
  constructor(
    private readonly httpService: HttpService,
    @InjectModel(HackerNews.name) private hackerNewsModel: Model<HackerNews>,
  ) {}

  @Cron('0 * * * *', {
    name: 'news',
    timeZone: 'America/Santiago',
  })
  async importHackerNews(): Promise<AxiosResponse<any>> {
    const response = await this.httpService.axiosRef.get(
      process.env.hackerNewsUrl,
    );
    const operations = response.data.hits.map((hit) => ({
      updateOne: {
        filter: { object_id: hit.objectID },
        update: {
          $set: {
            object_id: hit.objectID,
            created_at: hit.created_at,
            tite: hit.title,
            story_title: hit.story_title,
            url: hit.url,
            story_url: hit.story_url,
            author: hit.author,
          },
        },
        upsert: true,
      },
    }));
    await this.hackerNewsModel.bulkWrite(operations);
    return response.data;
  }

  async getAll() {
    return this.hackerNewsModel
      .find({
        shouldDisplay: true,
        $or: [{ title: { $ne: null } }, { story_title: { $ne: null } }],
      })
      .exec();
  }

  async blockNews({ objectId }: { objectId: string }) {
    return this.hackerNewsModel.updateOne(
      {
        object_id: objectId,
      },
      {
        $set: {
          shouldDisplay: false,
        },
      },
    );
  }
}
