import { Module } from '@nestjs/common';
import { HackerNewsService } from 'src/modules/hacker-news/services/app.service';
import { AppController } from 'src/modules/hacker-news/controllers/app.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { HackerNews, HackerNewsSchema } from './schemas/hacker-news.schema';
import { HttpModule } from '@nestjs/axios';

@Module({
  imports: [
    HttpModule,
    MongooseModule.forFeature([
      { name: HackerNews.name, schema: HackerNewsSchema },
    ]),
  ],
  controllers: [AppController],
  providers: [HackerNewsService],
})
export class HackerNewsModule {}
