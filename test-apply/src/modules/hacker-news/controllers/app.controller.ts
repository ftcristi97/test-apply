import { Controller, Get, Param, Put } from '@nestjs/common';
import { HackerNewsService } from '../services/app.service';
import { orderBy } from 'lodash';

@Controller('news')
export class AppController {
  constructor(private readonly appService: HackerNewsService) {}

  @Get()
  async getAll() {
    const allNews = await this.appService.getAll();
    return orderBy(allNews, ['created_at'], ['desc']);
  }

  // @Get('test')
  // testGetAll() {
  //   return this.appService.testGet();
  // }

  @Put(':id')
  update(@Param('id') id: string) {
    return this.appService.blockNews({ objectId: id });
  }
}
