import { Test, TestingModule } from '@nestjs/testing';
import { AppController } from './modules/hacker-news/controllers/app.controller';
import { MongoMemoryServer } from 'mongodb-memory-server';
import { Connection, connect, Model } from 'mongoose';
import { getModelToken } from '@nestjs/mongoose';
import { HackerNewsService } from './modules/hacker-news/services/app.service';
import {
  HackerNews,
  HackerNewsSchema,
} from './modules/hacker-news/schemas/hacker-news.schema';
import { HackerNewsFactory } from '../test/factories/hacker-news.factory';
import { HttpService } from '@nestjs/axios';
import { createMock, DeepMocked } from '@golevelup/ts-jest';
import { omit } from 'lodash';

describe('AppController', () => {
  let appController: AppController;
  let mongod: MongoMemoryServer;
  let mongoConnection: Connection;
  let hackerNewsModel: Model<HackerNews>;
  let httpService: DeepMocked<HttpService>;

  beforeAll(async () => {
    mongod = await MongoMemoryServer.create();
    const uri = mongod.getUri();
    mongoConnection = (await connect(uri)).connection;
    hackerNewsModel = mongoConnection.model(HackerNews.name, HackerNewsSchema);

    const app: TestingModule = await Test.createTestingModule({
      controllers: [AppController],
      providers: [
        HackerNewsService,
        { provide: getModelToken(HackerNews.name), useValue: hackerNewsModel },
        {
          provide: HttpService,
          useValue: createMock<HttpService>(),
        },
      ],
    }).compile();

    appController = app.get<AppController>(AppController);
    httpService = app.get(HttpService);
  });

  afterAll(async () => {
    await mongoConnection.dropDatabase();
    await mongoConnection.close();
    await mongod.stop();
  });

  afterEach(async () => {
    const collections = mongoConnection.collections;
    for (const key in collections) {
      const collection = collections[key];
      await collection.deleteMany({});
    }
  });

  describe('get all news', () => {
    it('should return all news from database', async () => {
      //Arrange.
      const mockedHackerNews = HackerNewsFactory.buildList(2, {
        shouldDisplay: true,
      });
      await new hackerNewsModel(mockedHackerNews[0]).save();
      await new hackerNewsModel(mockedHackerNews[1]).save();

      //Act.
      const articles = await appController.getAll();

      //Assert.
      const sanitizedArticles = articles.map((article) => ({
        object_id: article.object_id,
        title: article.title,
        story_title: article.story_title,
        url: article.url,
        story_url: article.story_url,
        author: article.author,
        shouldDisplay: article.shouldDisplay,
        created_at: article.created_at,
      }));
      expect(sanitizedArticles).toStrictEqual(mockedHackerNews);
    });
  });
});
